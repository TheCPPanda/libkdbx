from setuptools import setup, find_packages

setup(
    name='libkdbx',
    packages=find_packages(),
    author='Rico Lang',
    email='rico.lang15@gmail.com',
    install_requires=[
        'lxml>=4.2.5',
        'pycryptodome>=3.6.6',
        'python-dateutil>=2.7.5'
    ]
)
