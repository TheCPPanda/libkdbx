

class InvalidFile(Exception):
    """ Exception that gets thrown if the given file is an invalid kdbx file
    """
    pass


class FileVersionNotSupported(Exception):
    """ Exception that gets thrown if the file_signate is not supported
    """
    pass


class InvalidMasterKeyException(Exception):
    pass
