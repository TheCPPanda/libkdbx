import io

from contextlib import contextmanager
from .reader import KDBXReader


@contextmanager
def open(file, mode='brw', passphrase='', key_file=None):
    try:

        with io.open(file, mode=mode) as stream:
            kdbx_reader = KDBXReader(stream)
            yield kdbx_reader.read(passphrase.encode('utf-8'))
    finally:
        pass
