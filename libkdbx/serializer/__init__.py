import base64
from lxml import etree

from ..cryptography import xor
from ..models.password_database import PasswordDatabase


class KDBXSerializer:

    def __init__(self, binary_db, stream_generator):
        self.tree = etree.parse(binary_db)
        self.root = self.tree.getroot()
        self.stream_generator = stream_generator
        self.unprotect_passwords()

    def unprotect_passwords(self):
        for encrypted_password in self.root.findall('.//Value[@Protected="True"]'):
            b64pw = base64.b64decode(encrypted_password.text.encode('utf-8'))
            random_bytes = self.stream_generator.get_bytes(len(b64pw))
            password = xor(b64pw, random_bytes).decode('utf-8')

            encrypted_password.text = password
            encrypted_password.attrib['Protected'] = str(False)

    def serialize(self):
        raise Exception("Not implemented yet")

    def deserialize(self):
        root_tag = self.root.find('Root')
        return PasswordDatabase.from_xml(root_tag)

