from .cipher_factory import CipherFactory


class RandomStreamGenerator:

    def __init__(self, cipher_id, key):
        self.cipher = CipherFactory.get_stream_cipher(cipher_id, key)
        self.buffer = bytearray()

    def get_bytes(self, length):

        while len(self.buffer) < length:
            temp_buffer = self.cipher.encrypt(bytearray(64))
            self.buffer.extend(temp_buffer)

        portion = self.buffer[:length]
        del self.buffer[:length]
        return portion
