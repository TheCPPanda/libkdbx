from Crypto.Cipher import AES
from hashlib import sha256

from .cipher_factory import CipherFactory
from .random_stream_generator import RandomStreamGenerator


def generate_composite_key(*keys) -> bytes:
    """
    Generates the composite key out of the given keys
    """
    joined_keys = b''.join([sha256(key).digest() for key in keys])
    return sha256(joined_keys).digest()


def transform_key(composite_key, seed, rounds):
    """
    Transforms the composite key.

    Params:
        composite_key (bytes): The composite key to be transformed
        seed (bytes): The aes cipher seed
        rounds (int): Transformation rounds.
    
    Returns:
        The transformed composite key
    """
    cipher = AES.new(seed, AES.MODE_ECB)

    transformed_key = composite_key

    for n in range(0, rounds):
        transformed_key = cipher.encrypt(transformed_key)

    return sha256(transformed_key).digest()


def generate_master_key(master_seed, transformed_key):

    return sha256(master_seed + transformed_key).digest()


def xor(aa, bb):
    """Return a bytearray of a bytewise XOR of `aa` and `bb`."""
    return bytearray([a ^ b for a, b in zip(bytearray(aa), bytearray(bb))])
