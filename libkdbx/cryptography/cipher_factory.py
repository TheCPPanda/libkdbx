from enum import IntEnum
from Crypto.Cipher import AES, Salsa20


AES_CIPHER_ID = b'\x31\xC1\xF2\xE6\xBF\x71\x43\x50\xBE\x58\x05\x21\x6A\xFC\x5A\xFF'


class CryptoRandomStream(IntEnum):

    Nothing = 0,
    ArcFourVariant = 1,
    Salsa = 2


class CipherFactory:

    ciphers = {AES_CIPHER_ID: (AES, AES.MODE_CBC)}

    stream_ciphers = {
        CryptoRandomStream.Salsa: (Salsa20, bytes(bytearray.fromhex('e830094b97205d2a')))
    }

    @staticmethod
    def get_cipher(cipher_id, key, iv):
        cipher, mode = CipherFactory.ciphers[cipher_id]
        return cipher.new(key, mode, iv)

    @staticmethod
    def get_stream_cipher(cipher_id, key):
        cipher, nonce = CipherFactory.stream_ciphers[cipher_id]
        return cipher.new(key, nonce)
