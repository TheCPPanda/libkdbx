import struct
import zlib
import hashlib

from io import BytesIO
from collections import namedtuple

from .exceptions import InvalidFile, FileVersionNotSupported
from .models import Header, HeaderFieldId, KDBXFile
from .util import read_int, read_short, read_byte, unpad
from .serializer import KDBXSerializer
from .cryptography import (
    generate_composite_key,
    transform_key,
    generate_master_key,
    CipherFactory,
    RandomStreamGenerator
)


class FileSignature(namedtuple('Signature', ['magic_number', 'file_signature', 'file_version'])):

    MAGIC_NUMBER = 0x9AA2D903

    def __init__(self, *args, **kwargs):
        self.supported_versions = [0xB54BFB67]
        self.__validate__()

    def __new__(cls, *args, **kwargs):
        return super().__new__(cls, *args, **kwargs)

    def __validate__(self):
        if self.magic_number != self.MAGIC_NUMBER:
            raise InvalidFile('Invalid file kdbx file encountered')

        if self.file_signature not in self.supported_versions:
            raise FileVersionNotSupported('Your file version is not supported. Think about upgrading your KeePass version')

    def __str__(self):
        return f"Magicnumber: {hex(self.magic_number)} \nFile signature: {hex(self.file_signature)}"


class KDBXReader:

    def __init__(self, stream):
        self.stream = stream
        self.composite_key = None
        self.master_key = None
        self.raw_body = bytes()
        self._signature = None
        self._header = None

    def _read_signature(self):
        # Always make sure we started reading from the beginning
        self.stream.seek(0)
        return FileSignature(*struct.unpack('<III', self.stream.read(12)))

    def _read_header(self):
        header = Header()

        while True:

            field_id = read_byte(self.stream)
            length = read_short(self.stream)
            data = self.stream.read(length)

            if field_id == 0:
                break

            header[HeaderFieldId(field_id)] = data

        return header

    def generate_keys(self, *passphrases):
        self.composite_key = generate_composite_key(*passphrases)
        transformed_key = transform_key(
            self.composite_key,
            self._header.transform_seed,
            self._header.transform_rounds
            )

        self.master_key = generate_master_key(
            self._header.master_seed,
            transformed_key
        )

    def read(self, *passphrases):
        self._signature = self._read_signature()
        self._header = self._read_header()

        self.generate_keys(*passphrases)

        raw_bytes = self.stream.read()

        cipher = CipherFactory.get_cipher(
            self._header.cipher_id,
            self.master_key,
            self._header.encryption_iv)

        unencrypted_block_data = cipher.decrypt(raw_bytes)

        # Check if the first 32 bytes match if not TODO: Throw expcetion
        assert self._header.stream_start_bytes == unencrypted_block_data[:32]

        unblocked_stream = self._read_block_stream(unpad(unencrypted_block_data[32:]))

        if self._header.compression_flags == 1:
            d = zlib.decompressobj(16 + zlib.MAX_WBITS)
            unblocked_stream = BytesIO(d.decompress(unblocked_stream))

        cipher_id = self._header.inner_random_stream_id
        stream_key = hashlib.sha256(self._header.inner_random_stream_key).digest()
        stream_generator = RandomStreamGenerator(cipher_id, stream_key)

        serializer = KDBXSerializer(unblocked_stream, stream_generator)

        password_database = serializer.deserialize()
        kdbx_file = KDBXFile(
            signature=self._signature,
            header=self._header,
            master_key=self.master_key,
            password_database=password_database)

        return kdbx_file

    def _read_block_stream(self, stream) -> bytes:

        byte_stream = BytesIO(stream)

        # TODO: Actually verify the blocks
        block_id = read_int(byte_stream, 4)
        block_hash = byte_stream.read(32)
        data_length = read_int(byte_stream, 4)
        data = byte_stream.read(data_length)

        return data
