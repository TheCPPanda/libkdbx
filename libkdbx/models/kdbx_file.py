

class KDBXFile:

    def __init__(self, signature=None, header=None, master_key=None, password_database=None):
        self.signature = signature
        self.header = header
        self.master_key = master_key
        self.password_database = password_database
