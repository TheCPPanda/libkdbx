from libkdbx.models.times import Times
from .database_entry import BaseDatabaseEntry


class Entry(BaseDatabaseEntry):
    """
    """
    Title: str = ""
    Password: str = ""
    URL: str = ""
    UserName: str = ""

    @classmethod
    def from_xml(cls, xml):

        instance = cls()
        for child in xml:
            if child.tag == 'UUID':
                instance.uuid = child.text

            if hasattr(instance, child.tag):
                setattr(instance, child.tag, child.text)

            if child.tag == 'String':
                key, value = child
                setattr(instance, key.text, value.text)

            if child.tag == 'Times':
                instance.Times = Times.from_xml(child)

        return instance
