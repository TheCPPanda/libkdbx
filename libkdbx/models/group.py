from libkdbx.models.times import Times
from .database_entry import BaseDatabaseEntry, allowed_entities
from ..util import get_class_by_name


class Group(BaseDatabaseEntry):

    Name: str = ""

    def __init__(self):
        self.Name = ""
        super().__init__()

    def __iter__(self):
        return iter(self.entries)

    def __len__(self):
        return len(self.entries)

    @classmethod
    def from_xml(cls, xml):

        instance = cls()
        for child in xml:
            if child.tag == 'UUID':
                instance.uuid = child.text
            elif child.tag == 'Times':
                instance.Times = Times.from_xml(child)
            elif hasattr(instance, child.tag):
                setattr(instance, child.tag, child.text)

            elif child.tag in allowed_entities:
                module_name = child.tag.lower()
                class_name = child.tag
                entry = get_class_by_name(__package__, module_name, class_name).from_xml(child)

                if isinstance(entry, BaseDatabaseEntry):
                    instance.entries.append(entry)

        return instance
