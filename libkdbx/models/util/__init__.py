import re

COMPILED_STMT = re.compile('(.)([A-Z][a-z]+)')


class AttributeNameConverterMetaClass(type):

    def __to_python_name(name: str) -> str:
        return re.sub(COMPILED_STMT, r'\1_\2', name).lower()

    def __new__(cls, name, bases, body, **kwargs):
        new_body = body.copy()
        for key, value in body.items():
            if key[0].isupper():
                pythonic_name = cls.__to_python_name(key)
                fget = lambda self, key=key: getattr(self, key)
                fset = lambda self, val, key=key: setattr(self, key, val)
                new_body[pythonic_name] = property(fget, fset)

        return super().__new__(cls, name, bases, new_body, **kwargs)
