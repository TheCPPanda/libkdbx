from typing import List

from .database_entry import allowed_entities, BaseDatabaseEntry

from ..util import get_class_by_name


class PasswordDatabase:

    entries: List[BaseDatabaseEntry] = []

    def __init__(self):
        self.entries = []
        self.namespace = __package__

    def __len__(self):
        return len(self.entries)

    def __iter__(self):
        return iter(self.entries)

    @classmethod
    def from_xml(cls, tree):
        instance = cls()

        for child in tree:
            module_name = child.tag.lower()
            class_name = child.tag

            if class_name in allowed_entities:
                entry_cls = get_class_by_name(__package__, module_name, class_name)
                entry = entry_cls.from_xml(child)
                instance.entries.append(entry)

        return instance
