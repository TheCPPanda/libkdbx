import struct

from enum import IntEnum
from hashlib import sha256


class HeaderFieldId(IntEnum):

    END_OF_HEADER = 0
    COMMENT = 1
    CIPHER_ID = 2
    COMPRESSION_FLAGS = 3
    MASTER_SEED = 4
    TRANSFORM_SEED = 5
    TRANSFORM_ROUNDS = 6
    ENCRYPTION_IV = 7
    INNER_RANDOM_STREAM_KEY = 8
    STREAM_START_BYTES = 9
    INNER_RANDOM_STREAM_ID = 10


class Header(object):

    fmt = {
        HeaderFieldId.COMPRESSION_FLAGS: '<i',
        HeaderFieldId.TRANSFORM_ROUNDS: '<q',
        HeaderFieldId.INNER_RANDOM_STREAM_ID: '<i'
    }

    def __init__(self):
        self.header_fields = {}
        self._binary_header = bytearray()

    def __getattr__(self, name):
        if name.upper() not in HeaderFieldId.__members__:
            raise AttributeError(f'Header does not have the field {name}')

        header_id = HeaderFieldId[name.upper()]
        return self.header_fields.get(header_id, None)

    def __setitem__(self, name, value):
        if HeaderFieldId(name) in self.fmt:
            format_string = self.fmt[HeaderFieldId(name)]
            self.header_fields[name] = struct.unpack(format_string, value)[0]
        else:
            self.header_fields[name] = value

    def hash(self):
        return sha256(self._binary_header).digest()
