import base64
import uuid as stdlib_uuid

from typing import List

from .times import Times
from .util import AttributeNameConverterMetaClass

allowed_entities = [
    'Group',
    'Entry',
    'Times'
]


class BaseDatabaseEntry(metaclass=AttributeNameConverterMetaClass):

    Times: Times = None
    entries: List['BaseDatabaseEntry'] = []

    def __init__(self):
        self._uuid: stdlib_uuid.UUID = None
        self.entries = []

    @property
    def uuid(self):
        return self._uuid

    @uuid.setter
    def uuid(self, value):
        binary_uuid = bytes()
        if type(value) is str:
            binary_uuid = base64.b64decode(value)
        elif type(value) is bytes:
            binary_uuid = value

        self._uuid = stdlib_uuid.UUID(bytes=binary_uuid)
