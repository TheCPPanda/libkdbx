import datetime
import dateutil.parser
from lxml import etree


class Times:

    CreationTime: datetime.datetime = datetime.datetime.now()
    LastModificationTime: datetime.datetime = datetime.datetime.now()
    LastAccessTime: datetime.datetime = datetime.datetime.now()
    ExpiryTime: datetime.datetime = datetime.datetime.now()
    Expires: bool = False
    UsageCount: int = 0
    LocationChanged: datetime.datetime = datetime.datetime.now()

    def to_xml(self):

        root = etree.Element('Times')
        etree.SubElement(root, 'CreationTime').text = self.CreationTime.isoformat()
        etree.SubElement(root, 'LastModification').text = self.LastModificationTime.isoformat()
        etree.SubElement(root, 'LastAccessTime').text = self.LastAccessTime.isoformat()
        etree.SubElement(root, 'ExpiryTime').text = self.ExpiryTime.isoformat()
        etree.SubElement(root, 'Expires').text = str(self.Expires)
        etree.SubElement(root, 'UsageCount').text = str(self.UsageCount)
        etree.SubElement(root, 'LocationChanged').text = self.LocationChanged.isoformat()

        return root

    @classmethod
    def from_xml(cls, xml):

        instance = cls()

        for child in xml:
            if child.tag == 'Expires':
                instance.Expires = child.text.lower() == 'true'
            elif child.tag == 'UsageCount':
                instance.UsageCounter = int(child.text)
            else:
                setattr(instance, child.tag, dateutil.parser.parse(child.text))

        return instance
