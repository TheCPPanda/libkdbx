import struct
import importlib


def read_int(stream, length=1) -> int:
    """
    Reads length bytes from a file like object and converts it
    into an integer

    Args:
        stream (): A file like object
        length (int): The size of the integer

    Returns:
        The byte as integer
    """
    return struct.unpack('<I', stream.read(length))[0]


def read_byte(stream) -> int:
    """
    Reads one byte from a file like object and converts it
    into an integer

    Args:
        stream (): A file like object

    Returns:
        The byte as integer
    """
    return struct.unpack('<B', stream.read(1))[0]


def read_short(stream) -> int:
    """
    Reads two bytes from the stream and converts them to
    an integer.

    Args:
        stream: A file like object

    Returns:
        The two bytes as little endian integer
    """
    return struct.unpack('<H', stream.read(2))[0]


def unpad(data) -> bytes:
    return data[: -data[-1]]


def create_instance_by_name(namespace: str, module_name:str, class_name: str, *args, **kwargs):
    cls = get_class_by_name(namespace, module_name, class_name)
    return cls(*args, **kwargs)


def get_class_by_name(namespace: str, module_name:str, class_name: str):
    module = importlib.import_module("." + module_name, package=namespace)
    return getattr(module, class_name)
