import pytest
import os

from lxml import etree

from libkdbx.models import Times


@pytest.fixture
def password_database_xml():
    xml_path = os.path.join(os.path.dirname(__file__), 'data', 'pw.xml')
    return etree.parse(xml_path)


@pytest.fixture
def entry_xml():

    def create_string_elem(root, key, value):
        string = etree.SubElement(root, 'String')
        etree.SubElement(string, 'Key').text = key
        _value = etree.SubElement(string, 'Value').text = value

    root = etree.Element('Entry')
    etree.SubElement(root, 'UUID').text = 'z/k7qx3AGesO4IuT6mX68g=='
    create_string_elem(root, 'Title', 'test')
    create_string_elem(root, 'UserName', 'test_user')
    root.append(Times().to_xml())

    return root


@pytest.fixture
def test_file_path():
    return os.path.join(os.path.dirname(__file__), 'data', 'test.kdbx')
