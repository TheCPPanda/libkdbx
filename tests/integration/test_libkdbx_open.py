import libkdbx

from libkdbx.models import KDBXFile, PasswordDatabase, Header, Entry


def test_open_creates_kdbx_file_instance(test_file_path):

    with libkdbx.open(test_file_path, mode='rb', passphrase='123456') as f:
        assert isinstance(f, KDBXFile) is True
        assert isinstance(f.password_database, PasswordDatabase) is True
        assert f.master_key is not None
        assert isinstance(f.header, Header) is True
