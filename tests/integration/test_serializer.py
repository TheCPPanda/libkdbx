import uuid

from libkdbx.models import PasswordDatabase, Group, Entry, Times


def test_database_password_can_be_deserialized(password_database_xml):
    pw_elem = password_database_xml.find('.//Root')
    pw_database = PasswordDatabase.from_xml(pw_elem)

    assert isinstance(pw_database, PasswordDatabase)
    assert len(pw_database) == 1

    root_group = pw_database.entries[0]
    assert len(root_group) == 3


def test_group_can_be_created_from_xml(password_database_xml):

    group_elem = password_database_xml.find('.//Root/Group')
    group = Group.from_xml(group_elem)
    assert isinstance(group, Group)
    assert group.Name == 'Root'
    assert isinstance(group.uuid, uuid.UUID)
    assert len(group) == 3
    assert isinstance(group.Times, Times)


def test_deserialize_entry(entry_xml):
    entry = Entry.from_xml(entry_xml)

    assert isinstance(entry.uuid, uuid.UUID)
    assert entry.Title == 'test'
    assert entry.UserName == 'test_user'
    assert isinstance(entry.Times, Times)
