import pytest

from Crypto.Cipher._mode_cbc import CbcMode

from libkdbx.cryptography.cipher_factory import AES_CIPHER_ID, CipherFactory


@pytest.mark.parametrize('cipher_id, cipher_class', [(
        (AES_CIPHER_ID, CbcMode)
)])
def test_get_cipher_creates_object(cipher_id, cipher_class):
    assert isinstance(CipherFactory.get_cipher(cipher_id, b'0x3' * 8, None), cipher_class)
