import io
import pytest
import libkdbx.util


@pytest.fixture
def byte_stream():
    return io.BytesIO(b'\x04\x00\x00\x00')


def test_read_int(byte_stream):
    assert libkdbx.util.read_int(byte_stream, length=4) == 4
