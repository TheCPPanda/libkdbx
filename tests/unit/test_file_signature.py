import pytest

from libkdbx.reader import FileSignature
from libkdbx.exceptions import InvalidFile, FileVersionNotSupported

def test_file_signature_raises_invalid_file():

    with pytest.raises(InvalidFile):
        signature = FileSignature(magic_number=0x12345677, file_signature=3, file_version=3)


def test_file_signature_raises_not_supported():

    with pytest.raises(FileVersionNotSupported):
        signature = FileSignature(magic_number=FileSignature.MAGIC_NUMBER, file_signature=0x12345678, file_version=0)
